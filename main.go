package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"go.uber.org/zap"
)

const (
	appName    = "callme"
	appVersion = "1.0.0"
)

var log *zap.Logger

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	logger, _ := zap.NewProduction()
	defer logger.Sync()

	log = logger

	connDefaultDuration, err := time.ParseDuration("60m")
	if err != nil {
		fmt.Println(err)

		os.Exit(1)
	}

	serverAddr := flag.String("s", "", "Server addr")
	connNum := flag.Int("c", 10000, "Connection number")
	udpConn := flag.Bool("udp", false, "connection type udp")
	tcpConn := flag.Bool("tcp", false, "connection type tcp")
	connPort := flag.Int("p", 0, "Connection number")
	httpConn := flag.Bool("http", false, "by default https")
	connDuration := flag.Duration("d", connDefaultDuration, "Connection number")

	version := flag.Bool("v", false, "app version")

	flag.Parse()

	if *version {
		fmt.Println(appName, "version", appVersion)

		os.Exit(0)
	}

	if *serverAddr == "" {
		fmt.Println("Set target -s=localhost")
		os.Exit(1)
	}

	if *connPort != 0 && !*udpConn {
		*serverAddr = fmt.Sprintf("%s:%d", *serverAddr, *connPort)
	}

	log.Info("server: ", zap.String("addr", *serverAddr))
	log.Info("Conn", zap.Int("num", *connNum))

	for {
		select {
		case <-time.After(*connDuration):
			log.Info("Done!")

			return
		default:
			wg := sync.WaitGroup{}
			connNum := *connNum

			wg.Add(connNum)

			for i := 0; i < connNum; i++ {
				go func() {
					defer wg.Done()

					if *udpConn {
						callUDP(addrToBytes(*serverAddr), *connPort)
					} else if *tcpConn {
						callTCP(*serverAddr)
					} else {
						callHTTP(*httpConn, *serverAddr)
					}
				}()
			}

			wg.Wait()
		}
	}
}

func addrToBytes(addr string) []byte {
	addrArr := strings.Split(addr, ".")

	res := []int{}
	if len(addrArr) != 4 {
		log.Error("Invalide UDP IP")

		os.Exit(1)
	}

	for i := range addrArr {
		num, err := strconv.Atoi(addrArr[i])
		if err != nil {
			log.Error("can't transform str to int", zap.Error(err))
		}
		res = append(res, num)
	}
	return []byte{uint8(res[0]), uint8(res[1]), uint8(res[2]), uint8(res[3])}
}

func callUDP(ip []byte, port int) {
	Conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: ip, Port: port, Zone: ""})
	if err != nil {
		log.Error("net.DialUDP udp", zap.Error(err))

		return
	}

	defer Conn.Close()

	_, err = Conn.Write([]byte(gofakeit.Name()))
	if err != nil {
		log.Error("Write udp", zap.Error(err))

		return
	}

	log.Info("Connected UDP", zap.String("ip", string(ip)), zap.Int("port", port))
}

func callHTTP(httpProt bool, serverAddr string) {
	prot := "https"

	if httpProt {
		prot = "http"
	}

	url := fmt.Sprintf("%s://%s", prot, serverAddr)

	resp, err := http.Get(url)
	if err != nil {
		log.Error("GET HTTP", zap.Error(err))

		return
	}

	if resp.StatusCode != http.StatusOK {
		log.Error("Status not 200", zap.Error(err))
	}

	log.Info("Connected", zap.String("url", url))
}

func callTCP(servAddr string) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
	if err != nil {
		log.Error("ResolveTCPAddr failed", zap.Error(err))

		return
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Error("Dial failed", zap.Error(err))

		return
	}
	defer conn.Close()

	_, err = conn.Write([]byte(gofakeit.Name()))
	if err != nil {
		log.Error("Write to server failed", zap.Error(err))

		return
	}

	log.Info("Connected", zap.String("tcp", tcpAddr.IP.String()))
}
