module gitlab.com/forgeminiworld/callme

go 1.17

require (
	github.com/brianvoe/gofakeit/v6 v6.15.0
	go.uber.org/zap v1.21.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
